angular.module('mod.m31', []);

angular.module('mod.m31')
  .constant('MOD_demoMod', {
        API_URL: '',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m31', {
                url: '/demoMod',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m31.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_demoMod/views/menu/demoMod.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "demoMod"
               }
           })
            .state('m31.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_demoMod/views/dashboard/demoMod.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "demoMod"
                }

            })
            .state('m31.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_demoMod/views/dashboard/demoMod.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dummy Screen",
                    module: "demoMod"
                }

            })

            //FOR APP ADMIN
           .state('m31.admin', {
               url: '/admin',
               templateUrl: "mod_demoMod/views/admin/demoMod.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "demoMod"
               }
           })

    }])